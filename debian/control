Source: r-cran-ggforce
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-ggplot2 (>= 3.3.6),
               r-cran-rcpp,
               r-cran-scales,
               r-cran-mass,
               r-cran-tweenr,
               r-cran-gtable,
               r-cran-rlang,
               r-cran-polyclip,
               r-cran-tidyselect,
               r-cran-withr,
               r-cran-lifecycle,
               r-cran-cli,
               r-cran-vctrs,
               r-cran-systemfonts,
               r-cran-rcppeigen
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-ggforce
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-ggforce.git
Homepage: https://cran.r-project.org/package=ggforce
Rules-Requires-Root: no

Package: r-cran-ggforce
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: accelerating GNU R ggplot2
 The aim of 'ggplot2' is to aid in visual data investigations. This
 focus has led to a lack of facilities for composing specialised plots.
 'ggforce' aims to be a collection of mainly new stats and geoms that fills
 this gap. All additional functionality is aimed to come through the official
 extension system so using 'ggforce' should be a stable experience.
